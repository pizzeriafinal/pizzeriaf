package pe.edu.idat.pizzeria.model;

public abstract class ClienteModel {
	
	String cli_codigo;
	String cli_nombre;
	String cli_apellido;
	String cli_telefono;
	String cli_direccion;
	int cli_estado;
	public String getCli_codigo() {
		return cli_codigo;
	}
	public void setCli_codigo(String cli_codigo) {
		this.cli_codigo = cli_codigo;
	}
	public String getCli_nombre() {
		return cli_nombre;
	}
	public void setCli_nombre(String cli_nombre) {
		this.cli_nombre = cli_nombre;
	}
	public String getCli_apellido() {
		return cli_apellido;
	}
	public void setCli_apellido(String cli_apellido) {
		this.cli_apellido = cli_apellido;
	}
	public String getCli_telefono() {
		return cli_telefono;
	}
	public void setCli_telefono(String cli_telefono) {
		this.cli_telefono = cli_telefono;
	}
	public String getCli_direccion() {
		return cli_direccion;
	}
	public void setCli_direccion(String cli_direccion) {
		this.cli_direccion = cli_direccion;
	}
	public int getCli_estado() {
		return cli_estado;
	}
	public void setCli_estado(int cli_estado) {
		this.cli_estado = cli_estado;
	}
	
	
	
}
