package pe.edu.idat.pizzeria.bean;

import pe.edu.idat.pizzeria.model.ClienteModel;

public class ClienteBean extends ClienteModel {
	private static final long serialVersionUID = 1L;
	
	String cli_estdes;

	public String getCli_estdes() {
		return cli_estdes;
	}

	public void setCli_estdes(String cli_estdes) {
		this.cli_estdes = cli_estdes;
	}
	

}
