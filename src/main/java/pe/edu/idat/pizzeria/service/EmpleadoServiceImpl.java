package pe.edu.idat.pizzeria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.pizzeria.bean.EmpleadoBean;
import pe.edu.idat.pizzeria.dao.EmpleadoDAOImpl;



@Service("EmpleadoService")
public class EmpleadoServiceImpl  implements EmpleadoService{

	@Autowired
	private EmpleadoDAOImpl empleadoDAO;	
	
	@Override
	public List<EmpleadoBean> listarEmpleado() throws Exception {
		
		List<EmpleadoBean> lst =empleadoDAO.SelEmpleado();
		return lst;
	}

}
