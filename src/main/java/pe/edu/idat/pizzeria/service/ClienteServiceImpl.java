package pe.edu.idat.pizzeria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.pizzeria.bean.ClienteBean;
import pe.edu.idat.pizzeria.dao.ClienteDAOImpl;



@Service("ClienteService")
public class ClienteServiceImpl  implements ClienteService {

	@Autowired 
	private ClienteDAOImpl clienteDAO;
	
	@Override
	public List<ClienteBean> listarCliente() throws Exception {
		List<ClienteBean> lst =clienteDAO.SelCliente();
		return lst;
		
}
}	
	
