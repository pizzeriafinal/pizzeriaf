package pe.edu.idat.pizzeria.service;

import java.util.List;

import pe.edu.idat.pizzeria.bean.ClienteBean;

public interface ClienteService {
	
	public List<ClienteBean> listarCliente() throws Exception;
	

}
