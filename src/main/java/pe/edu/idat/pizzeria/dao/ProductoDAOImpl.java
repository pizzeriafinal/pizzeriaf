package pe.edu.idat.pizzeria.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.pizzeria.bean.ProductoBean;

public class ProductoDAOImpl implements ProductoDAO{

private JdbcTemplate jdbcTemplate;
	
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	@Override
	public List<ProductoBean> SelProducto() {

		List<ProductoBean> lst = null;
		String sql ="SELECT * FROM pizmaeproducto";
		
			
			lst = jdbcTemplate.query(sql, new ResultSetExtractor<List<ProductoBean>>(){
	            @Override
	            public List<ProductoBean> extractData(ResultSet rs) throws SQLException
	            {
	            	List<ProductoBean> list = new ArrayList<>();
	                while (rs.next())
	                {
	                	ProductoBean i = new ProductoBean();
	                	i.setPro_codigo(rs.getString("pro_codigo"));	                	
	                	i.setPro_nombre(rs.getString("pro_nombre"));
	                	i.setPro_marca(rs.getString("pro_marca"));
	                	i.setPro_estado(rs.getInt("pro_estado"));		                	
	                    list.add(i);
	                }
	                return list;
	            }

	        });	    
		
		
	    return lst;
		
		
	}

}
