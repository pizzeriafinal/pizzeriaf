package pe.edu.idat.pizzeria.dao;

import java.util.List;

import pe.edu.idat.pizzeria.bean.PedidoBean;

public interface PedidoDAO {
	
	public List<PedidoBean> SelPedido();

}
