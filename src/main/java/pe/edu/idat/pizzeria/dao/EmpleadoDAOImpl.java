package pe.edu.idat.pizzeria.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.pizzeria.bean.EmpleadoBean;

public class EmpleadoDAOImpl implements EmpleadoDAO{

private JdbcTemplate jdbcTemplate;
	
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	@Override
	public List<EmpleadoBean> SelEmpleado() {

		List<EmpleadoBean> lst = null;
		String sql ="SELECT * FROM pizmaeemplado";
		
			
			lst = jdbcTemplate.query(sql, new ResultSetExtractor<List<EmpleadoBean>>(){
	            @Override
	            public List<EmpleadoBean> extractData(ResultSet rs) throws SQLException
	            {
	            	List<EmpleadoBean> list = new ArrayList<>();
	                while (rs.next())
	                {
	                	EmpleadoBean i = new EmpleadoBean();
	                	i.setEmp_codigo(rs.getString("emp_codigo"));	                	
	                	i.setEmp_nombre(rs.getString("emp_nombre"));
	                	i.setEmp_direccion(rs.getString("emp_direccion"));
	                	i.setEmp_estado(rs.getInt("emp_estado"));		                	
	                    list.add(i);
	                }
	                return list;
	            }

	        });	    
		
		
	    return lst;
		
		
	}

}
