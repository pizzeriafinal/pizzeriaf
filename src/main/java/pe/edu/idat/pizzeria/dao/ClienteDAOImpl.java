package pe.edu.idat.pizzeria.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.pizzeria.bean.ClienteBean;

public class ClienteDAOImpl  implements ClienteDAO{
	
	private JdbcTemplate jdbcTemplate;
	
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	@Override
	public List<ClienteBean> SelCliente() {

		List<ClienteBean> lst = null;
		String sql ="SELECT * FROM pizmaecliente";
		
			
			lst = jdbcTemplate.query(sql, new ResultSetExtractor<List<ClienteBean>>(){
	            @Override
	            public List<ClienteBean> extractData(ResultSet rs) throws SQLException
	            {
	            	List<ClienteBean> list = new ArrayList<>();
	                while (rs.next())
	                {
	                	ClienteBean i = new ClienteBean();
	                	i.setCli_codigo(rs.getString("cli_codigo"));	                	
	                	i.setCli_nombre(rs.getString("cli_nombre"));
	                	i.setCli_direccion(rs.getString("cli_direccion"));
	                	i.setCli_estado(rs.getInt("cli_estado"));		                	
	                    list.add(i);
	                }
	                return list;
	            }

	        });	    
		
		
	    return lst;
		
		
	}
}
