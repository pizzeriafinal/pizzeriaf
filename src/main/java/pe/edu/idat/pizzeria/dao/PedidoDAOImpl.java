package pe.edu.idat.pizzeria.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.pizzeria.bean.PedidoBean;

public class PedidoDAOImpl implements PedidoDAO{

private JdbcTemplate jdbcTemplate;
	
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	@Override
	public List<PedidoBean> SelPedido() {

		List<PedidoBean> lst = null;
		String sql ="SELECT * FROM pizmaepedido";
		
			
			lst = jdbcTemplate.query(sql, new ResultSetExtractor<List<PedidoBean>>(){
	            @Override
	            public List<PedidoBean> extractData(ResultSet rs) throws SQLException
	            {
	            	List<PedidoBean> list = new ArrayList<>();
	                while (rs.next())
	                {
	                	PedidoBean i = new PedidoBean();
	                	i.setPed_codigo(rs.getString("ped_codigo"));	                	
	                	i.setPed_tpedido(rs.getString("ped_tpedido"));
	                	i.setPed_npedido(rs.getInt("ped_npedido"));
	                	i.setPed_descripcion(rs.getString("ped_descripcion"));		                	
	                    list.add(i);
	                }
	                return list;
	            }

	        });	    
		
		
	    return lst;
		
		
	}

}
