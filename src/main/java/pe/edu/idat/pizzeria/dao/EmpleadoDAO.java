package pe.edu.idat.pizzeria.dao;

import java.util.List;

import pe.edu.idat.pizzeria.bean.EmpleadoBean;

public interface EmpleadoDAO {
	
	public List<EmpleadoBean> SelEmpleado();

}
