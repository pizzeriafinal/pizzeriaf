<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

Vista de Empleado


<table id="data-table-default" class="table table-striped table-bordered">
						<thead>							
							<th></th>
							<th>Codigo</th>							
							<th>nombre</th>							
							<th>apellido1</th>
							<th>apellido2</th>
							<th>telefono</th>
							<th>direccion</th>
							<th>estado</th>
							<th></th>
						</thead>
						<tbody>
										                 
							<c:forEach var="item" items="${listadoEmpleado}" varStatus="status">
							<tr>
							  
							    <td>${item.emp_codigo}</td>							    
							    <td>${item.emp_nombre}</td>
							    <td>${item.emp_apellido1}</td>
							    <td>${item.emp_apellido2}</td>
							    <td>${item.emp_telefono}</td>
							    <td>${item.emp_direccion}</td>
							    <td>${item.emp_estado}</td>
							    <td></td>
							    <td>
							    	<button class="btn btn-xs btn-danger" id="btnPageDeleteRow"><i class="fa fa-trash-alt"></i></button>
							    	<button class="btn btn-xs btn-primary" id="btnPageEditRow"><i class="fa fa-pencil-alt"></i></button>
							    </td>
							 </tr>
							 </c:forEach>
					
						</tbody>
					</table>





</body>
</html>