<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<table id="data-table-default" class="table table-striped table-bordered">
						<thead>							
							<th></th>
							<th>codigo</th>							
							<th>rsocial</th>							
							<th>telefono</th>
							<th>estado</th>
							
							<th></th>
						</thead>
						<tbody>
										                 
							<c:forEach var="item" items="${listadoProveedor}" varStatus="status">
							<tr>
							    <td>${status.index + 1}</td>
							    <td>${item.pvr_codigo}</td>							    
							    <td>${item.pvr_rsocial}</td>
							    <td>${item.pvr_telefono}</td>
							     <td>${item.pvr_estado}</td>
							    <td></td>
							    <td>
							    	<button class="btn btn-xs btn-danger" id="btnPageDeleteRow"><i class="fa fa-trash-alt"></i></button>
							    	<button class="btn btn-xs btn-primary" id="btnPageEditRow"><i class="fa fa-pencil-alt"></i></button>
							    </td>
							 </tr>
							 </c:forEach>
					
						</tbody>
					</table>
					<!-- FIN S502CI -->

</body>
</html>